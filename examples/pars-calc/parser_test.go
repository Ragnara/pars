package main

import (
	"testing"
)

func TestParser(t *testing.T) {
	tests := []struct {
		s      string
		result Number
	}{
		{"0", 0},
		{"15.29705854", 15.29705854},
		{"1+2+3", 6},
		{"1*2*3", 6},
		{"1+2*3-4/5", 6.2},
		{"4-5+1", 0},
		{"1-2-3-4-5-6-7-8-9", 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9},
		{"4/5*2", 1.6},
		{"1/2/3/4/5", 1.0 / 2 / 3 / 4 / 5},
	}

	for _, test := range tests {
		e, err := ParseCalculation(test.s)
		if err != nil {
			t.Errorf("Unexpected parsing error for calculation '%v': %v", test.s, err)
			continue
		}

		actual := e.Eval()
		if actual != test.result {
			t.Errorf("%v: Expected %v, got %v", test.s, test.result, actual)
		}
	}
}

func TestErrorMessages(t *testing.T) {
	tests := []struct {
		s   string
		err string
	}{
		{"a", "number expected"},
		{"1+", "addition expected: number expected"},
		{"1-", "subtraction expected: number expected"},
		{"1*", "multiplication expected: number expected"},
		{"1/", "division expected: number expected"},
		{"1+2*", "addition expected: multiplication expected: number expected"},
		{"1 2", "Expected EOF: Found byte 0x32"},
	}

	for _, test := range tests {
		_, err := ParseCalculation(test.s)
		if err == nil {
			t.Errorf("Unexpected success parsing calculation '%v': Expected error: %v", test.s, test.err)
		}

		if err.Error() != test.err {
			t.Errorf("%v: Expected error '%v', got '%v'", test.s, test.err, err)
		}
	}
}
