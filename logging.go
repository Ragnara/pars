package pars

import (
	"log"
	"os"
)

//Logger is anything that lines can be printed to.
type Logger interface {
	Println(...interface{})
}

type loggingParser struct {
	Parser
	logger Logger
}

//WithLogging wraps a parser so that calls to it are logged to a given logger.
func WithLogging(parser Parser, logger Logger) Parser {
	return &loggingParser{Parser: parser, logger: logger}
}

//WithStdLogging wraps a parser so that calls to it are logged to a logger logging to StdErr with a given prefix.
func WithStdLogging(parser Parser, prefix string) Parser {
	logger := log.New(os.Stderr, prefix, log.LstdFlags)
	return WithLogging(parser, logger)
}

func (l *loggingParser) Parse(src *Reader) (interface{}, error) {
	l.logger.Println("IN: Parse")
	val, err := l.Parser.Parse(src)
	if err == nil {
		l.logger.Println("OUT: Parse")
	} else {
		l.logger.Println("OUT: Parse with err", err.Error())
	}
	return val, err
}

func (l *loggingParser) Unread(src *Reader) {
	l.logger.Println("IN: Unread")
	l.Parser.Unread(src)
	l.logger.Println("OUT: Unread")
}

func (l *loggingParser) Clone() Parser {
	return &loggingParser{Parser: l.Parser.Clone(), logger: l.logger}
}
