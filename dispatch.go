package pars

import (
	"reflect"
)

type dispatchParser struct {
	clauses []DispatchClause
	parsers []Parser
}

//Dispatch returns a parser that is like a combination of Seq and Or with limited backtracking.
//
//A Dispatch contains multiple clauses consisting of parsers. Dispatch parses by trying the clauses one by one.
//The first matching clause is used, later clauses are not tried. Each clause can contain multiple parsers.
//Clauses are special because they limit the backtracking: If the first parser of a clause matches, that clause
//is selected even if a later parser of that clause fails.
//If no clause matches, the error from the last clause is returned.
//
//The motivation for limited backtracking is in better error reporting. When an Or parser fails, all you know is that
//not a single parser succeeded. When a Dispatch parser fails after a clause was selected, you know which subclause
//was supposed to be parsed and can return a fitting error message.
func Dispatch(clauses ...DispatchClause) Parser {
	return &dispatchParser{clauses: clauses}
}

func (d *dispatchParser) Parse(src *Reader) (interface{}, error) {
	var err error = dispatchWithoutMatch{}
	for _, clause := range d.clauses {
		parsers := clause.Parsers()
		if len(parsers) == 0 {
			continue
		}

		var val []interface{}
		var selected bool
		val, selected, err = d.tryParse(src, parsers)
		if selected {
			if err != nil {
				return nil, clause.TransformError(err)
			}
			return clause.TransformResult(val), nil
		}
	}
	return nil, err
}

func (d *dispatchParser) tryParse(src *Reader, parsers []Parser) ([]interface{}, bool, error) {
	first := parsers[0].Clone()
	val, err := first.Parse(src)
	if err != nil {
		return nil, false, err
	}

	ownParsers := make([]Parser, 0, len(parsers))
	ownParsers = append(ownParsers, first)

	vals := make([]interface{}, len(parsers))
	vals[0] = val

	for i, parser := range parsers {
		if i == 0 {
			continue
		}

		ownParsers = append(ownParsers, parser.Clone())

		vals[i], err = ownParsers[i].Parse(src)
		if err != nil {
			unreadParsers(ownParsers[:i], src)
			return nil, true, err
		}
	}

	d.parsers = ownParsers
	return vals, true, nil
}

func (d *dispatchParser) Unread(src *Reader) {
	unreadParsers(d.parsers, src)
	d.parsers = nil
}

func (d *dispatchParser) Clone() Parser {
	return &dispatchParser{clauses: cloneClauses(d.clauses)}
}

type dispatchSomeParser struct {
	clauses []DispatchClause
	parsers []Parser
}

//DispatchSome is a parser that combines Dispatch and Some. Like Dispatch, it tries to find a
//clause where the first parser succeeds and then commits to that clause. If the whole clause
//succeeds, the TransformResult method is called as usual. If a committed clause fails, the
//whole parser fails and returns the corresponding error (transformed by the TransformError
//method).
//
//Unlike Dispatch, after a successful parse another round starts with the first clause again.
//DispatchSome parses until either an error occurs or no single clause matched. Different from
//Dispatch, if no clause matches, it is not an error but marks the end of the loop.
//
//A successful DispatchSome returns a slice of all single results from each Dispatch round. This
//slice may be empty.
func DispatchSome(clauses ...DispatchClause) Parser {
	clauses = append(clauses, exitClause)
	return &dispatchSomeParser{clauses: clauses}
}

func (d *dispatchSomeParser) Parse(src *Reader) (interface{}, error) {
	results := make([]interface{}, 0, 16)
	for i := 0; ; i++ {
		freshClauses := cloneClauses(d.clauses)
		parser := Dispatch(freshClauses...)

		val, err := parser.Parse(src)
		if err != nil {
			unreadParsers(d.parsers[:i], src)
			return nil, err
		}

		if val == exitClause {
			return results, nil
		}

		d.parsers = append(d.parsers, parser)
		results = append(results, val)
	}
}
func (d *dispatchSomeParser) Unread(src *Reader) {
	unreadParsers(d.parsers, src)
	d.parsers = nil
}

func (d *dispatchSomeParser) Clone() Parser {
	return &dispatchSomeParser{clauses: cloneClauses(d.clauses)}
}

func cloneClauses(clauses []DispatchClause) []DispatchClause {
	result := make([]DispatchClause, 0, len(clauses))
	for _, clause := range clauses {
		if clonable, ok := clause.(ClonableDispatchClause); ok {
			result = append(result, clonable.Clone())
		} else {
			result = append(result, clause)
		}
	}

	return result
}

type exitClauseType struct{}

var exitClause exitClauseType

func (e exitClauseType) Parsers() []Parser {
	return []Parser{nop}
}

func (e exitClauseType) TransformResult(_ []interface{}) interface{} {
	return e
}

func (e exitClauseType) TransformError(err error) error {
	return err
}

//DispatchClause is the interface of a clause used by Dispatch.
type DispatchClause interface {
	//Parsers returns the parsers of the clause.
	Parsers() []Parser
	//TransformResult allows the DispatchClause to combine the results of its parsers to a single result.
	TransformResult([]interface{}) interface{}
	//TransformError allows the DispatchClause to replace or extend the error returned on a failed match.
	TransformError(error) error
}

//ClonableDispatchClause is the interface of stateful DispatchClause types that need clones with unique state.
type ClonableDispatchClause interface {
	DispatchClause
	//Clone creates a functionally equivalent clause with unique state.
	Clone() ClonableDispatchClause
}

//Clause is the most simple DispatchClause. It is just a slice of parsers without any transformations.
type Clause []Parser

var _ DispatchClause = Clause{}

//Parsers returns the parser slice for this clause.
func (c Clause) Parsers() []Parser {
	return c
}

//TransformResult returns the only value if the slice of values has only one element.
//Otherwise it returns the slice of values unchanged.
func (c Clause) TransformResult(val []interface{}) interface{} {
	if len(val) == 1 {
		return val[0]
	}
	return val
}

//TransformError returns the given error unchanged.
func (c Clause) TransformError(err error) error {
	return err
}

//DescribeClause extends the error message of a clause so that a custom description is part of the message.
type DescribeClause struct {
	DispatchClause
	Description string
}

//TransformError extends the error message of a clause so that a custom description is part of the message.
func (d DescribeClause) TransformError(err error) error {
	return describeClauseError{description: d.Description, innerError: err}
}

//Clone calls clone of the inner clause if implemented.
func (d DescribeClause) Clone() DispatchClause {
	if clonable, ok := d.DispatchClause.(ClonableDispatchClause); ok {
		return DescribeClause{DispatchClause: clonable.Clone(), Description: d.Description}
	}

	return d
}

//StringJoiningClause extends a clause that consists of parsers that return runes or strings so that it
//returnes a single string instead. Slices are handled recursively.
//
//StringJoiningClause WILL PANIC if any of the parsers return something other than a rune or a string or a
//slice of these types.
type StringJoiningClause struct {
	DispatchClause
}

//TransformResult joins runes and strings together like JoinString.
func (s StringJoiningClause) TransformResult(vals []interface{}) interface{} {
	val, _ := joinToString(vals)
	return val
}

//Clone calls clone of the inner clause if implemented.
func (s StringJoiningClause) Clone() DispatchClause {
	if clonable, ok := s.DispatchClause.(ClonableDispatchClause); ok {
		return StringJoiningClause{DispatchClause: clonable.Clone()}
	}

	return s
}

//InsteadOfClause extends a clause so that it returns a fixed value as if it was wrapped in an InsteadOf.
type InsteadOfClause struct {
	DispatchClause
	Result interface{}
}

//TransformResult returns the Result as if the clause was wrapped in an InsteadOf.
func (i InsteadOfClause) TransformResult(_ []interface{}) interface{} {
	return i.Result
}

//Clone calls clone of the inner clause if implemented.
func (i InsteadOfClause) Clone() DispatchClause {
	if clonable, ok := i.DispatchClause.(ClonableDispatchClause); ok {
		return InsteadOfClause{DispatchClause: clonable.Clone(), Result: i.Result}
	}

	return i
}

//InsteadOfDerefClause extends a clause so that it returns a value that will be dereferenced
//as if the clause was wrapped in an InsteadOfDeref.
type InsteadOfDerefClause struct {
	DispatchClause
	Result interface{}
}

//TransformResult returns the dereferenced Result as if the clause was wrapped in an InsteadOfDeref.
func (i InsteadOfDerefClause) TransformResult(_ []interface{}) interface{} {
	return reflect.ValueOf(i.Result).Elem().Interface()
}

//Clone calls clone of the inner clause if implemented.
func (i InsteadOfDerefClause) Clone() DispatchClause {
	if clonable, ok := i.DispatchClause.(ClonableDispatchClause); ok {
		return InsteadOfDerefClause{DispatchClause: clonable.Clone(), Result: i.Result}
	}

	return i
}
