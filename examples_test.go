package pars

import (
	"fmt"
	"strings"
	"unicode"
)

func ExampleDiscardLeft() {
	data := "$123"

	dollarParser := DiscardLeft(Char('$'), Int())

	result, err := ParseString(data, dollarParser)
	if err != nil {
		fmt.Println("Error while parsing:", err)
		return
	}

	fmt.Printf("%v: %T\n", result, result)

	//Output:
	//123: int
}

func ExampleSeq() {
	data := "$123"

	dollarParser := Seq(Char('$'), Int())

	result, err := ParseString(data, dollarParser)
	if err != nil {
		fmt.Println("Error while parsing:", err)
		return
	}

	values := result.([]interface{})
	fmt.Printf("%c: %T\n", values[0], values[0])
	fmt.Printf("%v: %T\n", values[1], values[1])

	//Output:
	//$: int32
	//123: int
}

func ExampleOr() {
	data := "124"

	parser := Or(String("123"), String("124"))

	result, err := ParseString(data, parser)
	if err != nil {
		fmt.Println("Error while parsing:", err)
		return
	}

	fmt.Printf("%v: %T\n", result, result)

	//Output:
	//124: string
}

func ExampleScanner() {
	data := "this is a text of words"
	reader := NewReader(strings.NewReader(data))

	wordParser := SwallowTrailingWhitespace(JoinString(RunesUntil(CharPred(unicode.IsSpace))))

	scanner := NewScanner(reader, wordParser)

	for scanner.Scan() {
		fmt.Println(scanner.ResultString())
	}
	fmt.Println(scanner.Err())

	//Output:
	//this
	//is
	//a
	//text
	//of
	//words
	//<nil>
}

func ExampleAssignInto() {
	data := "My name is 'Ark'   "

	var name string
	parser := Seq(String("My name is"), Into(SwallowWhitespace(RuneDelimitedString('\'', '\'')), AssignInto(&name)), EOF)

	_, err := ParseString(data, parser)
	if err != nil {
		fmt.Println("Error while parsing:", err)
		return
	}

	fmt.Printf("Hello, %v!\n", name)

	//Output:
	//Hello, Ark!
}

func ExampleAssignIntoSlice() {
	data := "123,456,789"

	var numbers []int

	parser := Seq(Into(Sep(Int(), Char(',')), AssignIntoSlice(&numbers)), EOF)

	_, err := ParseString(data, parser)
	if err != nil {
		fmt.Println("Error while parsing:", err)
		return
	}

	fmt.Printf("Type safe numbers from parsing:\n%#v\n", numbers)

	var sum int
	for _, number := range numbers {
		sum += number
	}

	fmt.Println("Sum:", sum)

	//Output:
	//Type safe numbers from parsing:
	//[]int{123, 456, 789}
	//Sum: 1368
}
